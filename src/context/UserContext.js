import React from "react";
import firebase from '../firebase';

var UserStateContext = React.createContext();
var UserDispatchContext = React.createContext();

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return { ...state, isAuthenticated: true };
    case "SIGN_OUT_SUCCESS":
      return { ...state, isAuthenticated: false };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function UserProvider({ children }) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: !!localStorage.getItem("id_token"),
  });

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

function useUserState() {
  var context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

function useUserDispatch() {
  var context = React.useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error("useUserDispatch must be used within a UserProvider");
  }
  return context;
}

export { UserProvider, useUserState, useUserDispatch, loginUser, signOut };

// ###########################################################

  function loginUser(dispatch, login, password, history, setIsLoading, setError) {
  setError(false);
  setIsLoading(true);

  if (!!login && !!password) {
    setTimeout(() => {
      //localStorage.setItem('id_token', 1)
      setError(null)
      setIsLoading(false)
      ////////////////////////////////////////////
      
      firebase
        .auth()
        .signInWithEmailAndPassword(login ,password)//change this to email,password
        .then(() => {
          firebase.firestore().collection("admin").doc(login).get().then(function(doc) {
            if (doc.exists) {
              dispatch({ type: 'LOGIN_SUCCESS' })
              history.push('/app/requests')//
          } else {
             alert("Sorry!You are not Authorized")
          }
        })
      })
        .catch(error => {if(error.code=="auth/wrong-password"||error.code=="auth/invalid-email"){
          alert("Username or Password is Incorrect");
        }else if(error.code=="auth/user-not-found"){
          alert("Sorry!You are Not Registered");
        }else if(error.code=="auth/network-request-failed"){
          alert("oops!You're offline")
        }else{
          alert(error.code);
        }
              setIsLoading(false);})
        
       // return ;
      //////////////////////////////////////////
      
    }, 2000);
  } else {
   // dispatch({ type: "LOGIN_FAILURE" });
    setError(true);
    setIsLoading(false);
  alert("Something went wrong :( Please try again!")
  }
}

function signOut(dispatch, history) {
  localStorage.removeItem("id_token");
  dispatch({ type: "SIGN_OUT_SUCCESS" });
  history.push("/login");
}

function abc(username){
  console.log("abc",username);
  firebase.firestore().collection('admin').doc(username).set({
    email: username,
    username:username,
    admin:true,
    })
}



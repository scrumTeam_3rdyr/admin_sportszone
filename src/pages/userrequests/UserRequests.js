import React from "react";
import { Grid ,TextareaAutosize} from "@material-ui/core";
import Popup from "reactjs-popup";

// components
import PageTitle from "../../components/PageTitle/PageTitle";
import Widget from "../../components/Widget/Widget";


// data
import firebase from '../../firebase';

import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
} from "@material-ui/core";
import { Button } from "../../components/Wrappers";


import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/SportsFootball';
import WorkIcon from '@material-ui/icons/Work';
import EmailIcon from '@material-ui/icons/Email';
import PhoneContactIcon from '@material-ui/icons/ContactPhone';
import styles from "../../components/PageTitle/styles";

const listStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
}));



const states = {
  sent: "success",
  pending: "warning",
  declined: "secondary",
};


const dbref=firebase.firestore().collection('court'); 
const table=[];/*
const data=dbref.where('status','==','unreg').get()
    .then(querySnapshot => {
      const data = querySnapshot.docs.map(doc => doc.data());
      console.log(data); // array of cities objects
    });
  
    
export default function UserRequests() {
  return (
    <>
      <PageTitle title="User Requests" />
      <Grid container spacing={4}>
        <TextareaAutosize>{data}</TextareaAutosize>
        <Grid item xs={12}>
          <Widget title="New Requests" upperTitle noBodyPadding>
            <Table data={mock.table} />
          </Widget>
        </Grid>
      </Grid>
    </>
  );
}*/

export default class UserRequests extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      courts: [],
      show: false
    };
  }

  toggleShow = show => {
    this.setState({show});
  }

  /*componentDidMount() { //this was working well..for realtime updates i changed the code.if its not working,move here!
    dbref
    .where('active','==',false)
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());
        console.log(data);
        this.setState({ courts: data });

      })
  }*/

  componentDidMount() {
    dbref
    .where('active','==',false)
      .onSnapshot(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());
        console.log(data);
        this.setState({ courts: data });

      })
  }
  

  makeRegistered(emailID){
    //alert("Successfully Registered the User "+emailID);
    dbref.doc(emailID).update({
      active:true,
    })
    .then(() => alert("Successfully Registered the User "+emailID))
    //.then(()=>window.location.reload())
    .catch(error=>alert(error.code))
  }

  makeDeleted(emailID){
    //alert("Successfully Registered the User "+emailID);
    //enter a popup dialog here to confirm deletion
    //alert("Once you delete it cannot be undone..")
    dbref.doc(emailID).delete()
    .then(() => alert("Successfully Deleted the User"))
    //.then(()=>window.location.reload())
    .catch(error=>alert(error.code))
  }

  render() {
    const { courts } = this.state;
    courts.forEach(element => {
      
    });
    const {show} = this.state;
    return (
      <>
    <PageTitle title="User Requests"/>
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <Widget title="New Requests" upperTitle noBodyPadding>
          <Table className="mb-0">
            <TableHead>
              <TableRow >
                <TableCell> NAME </TableCell>
                <TableCell> EMAIL </TableCell>
                <TableCell>ADDRESS </TableCell>
                </TableRow>
                </TableHead>
            <TableBody>
            {courts.map(court => (
          <TableRow key={court.uid}>
            <TableCell>{court.court_name}</TableCell>
            <TableCell>{court.email}</TableCell>
            <TableCell>{court.address}</TableCell>
            <TableCell>
            <Popup trigger={<Button
              color={'warning'}
              size="small"
              className="px-2"
              variant="contained"
              >
              VIEW
              </Button>}
              modal
              closeOnDocumentClick
            >

  <Card >
    <CardActionArea>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {court.name}
        </Typography>

        <List className={listStyles.root}>
        <ListItem>
        <img src={court.imageuri} alt="court image" style={{height:100,aspectRatio:"auto"}} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <ImageIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Court" secondary={court.court_name} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <WorkIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Address" secondary={court.address} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <PhoneContactIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Phone" secondary={court.contact_no} />
      </ListItem>
      <ListItem>
        <ListItemAvatar>
          <Avatar>
            <EmailIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Email" secondary={court.email} />
      </ListItem>
    </List>

      </CardContent>
    </CardActionArea>
    <CardActions>
    <Button
      color={'primary'}
      size="small"
      className="px-2"
      variant="contained"
      //onClick={() => {alert("hello"+court.email)}}
      onClick={() => { this.makeRegistered(court.email) }}
    >
                ADD
              </Button>

              <Popup trigger={<Button
              color={'warning'}
              size="small"
              className="px-2"
              variant="contained"
              >
              DELETE
              </Button>}
              modal
              closeOnDocumentClick
            >

  <Card >
    <CardActionArea>
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          Delete {court.name} ?
        </Typography>
                Are you Sure you want to delete?
                This cannot be undone
      </CardContent>
    </CardActionArea>
    <CardActions>

              <Button
      color={'secondary'}
      size="small"
      className="px-2"
      variant="contained"
      onClick={() => {this.makeDeleted(court.email)}}
    >
                DELETE
              </Button>
          
    </CardActions>
  </Card>


            </Popup>
            
    </CardActions>
  </Card>


            </Popup>
              
              </TableCell>
              <TableCell>
              <Button
                color={'secondary'}
                size="small"
                className="px-2"
                variant="contained"
                //onClick={() => {alert("hello"+court.email)}}
                onClick={() => { this.makeRegistered(court.email) }}
              >
                ADD
              </Button>
            </TableCell>
          </TableRow>
        ))}
            </TableBody>


            </Table>
          </Widget>
        </Grid>
      </Grid>
      
    </>
    );
  }
}


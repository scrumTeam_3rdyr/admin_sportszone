import React from "react";
import { Grid } from "@material-ui/core";
import MUIDataTable from "mui-datatables";
import Popup from "reactjs-popup";
// components
import PageTitle from "../../components/PageTitle/PageTitle";
import Widget from "../../components/Widget/Widget";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
// data

// data
import firebase from '../../firebase';
import {
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
} from "@material-ui/core";
import { Button } from "../../components/Wrappers";
const dbref=firebase.firestore().collection('court'); 



//const datatableData = [
 // ["Joe James", "Example Inc.", "Yonkers", "NY"],
 // ["John Walsh", "Example Inc.", "Hartford", "CT"],

//];

export default class Reports extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      courts: []
    };
  }
  
  componentDidMount() {
    dbref
    .where('active','==',true)
      .get()
      .then(querySnapshot => {
        const data = querySnapshot.docs.map(doc => doc.data());
        console.log("data" ,data);
        this.setState({ courts: data });
  
      })
  }
  
  makeDeleted(emailID){
    //alert("Successfully Registered the User "+emailID);
    //enter a popup dialog here to confirm deletion
    alert("Once you delete it cannot be undone..")
    dbref.doc(emailID).delete()
    .then(() => alert("Successfully Deleted the User"))
  //  .then(()=>window.location.reload())
    .catch(error=>alert(error.code))
  }

  makeDisabled(emailID){
    //alert("Successfully Registered the User "+emailID);
    dbref.doc(emailID).update({
      active:false,
    })
    .then(() => alert("Successfully Disabled the account"))
    //.then(()=>window.location.reload())
    .catch(error=>alert(error.code))
  }

  render() {
    const { courts } = this.state;
    //const datatableData=courts;
    const arr=[];
    //console.log("datatabledata :",datatableData)
    courts.forEach(element => {
      arr.push(
        [element.court_name,element.address,element.email,element.contact_no]
      )
    });
    console.log("array :",arr[0]);
    return (
      <>
        <PageTitle title="Reports" />
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <MUIDataTable
              title="Active Users Report"
              data={arr}
              
              columns={["Name", "Address", "Email", "Phone"]}
              options={{
                filterType: "checkbox",
              }}
            />
          </Grid>
        
        </Grid>
      </>
    );
  }
}

import React from "react";
import {
  Route,
  Switch,
  Redirect,
  withRouter,
} from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Header";
import Sidebar from "../Sidebar";

// pages

import UserRequests from "../../pages/userrequests/UserRequests";

import Reports from "../../pages/reports/Reports";

import ManageUsers from "../../pages/manageusers/ManageUsers"


// context
import { useLayoutState } from "../../context/LayoutContext";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
        <>
          <Header history={props.history} />
          <Sidebar />
          <div
            className={classnames(classes.content, {
              [classes.contentShift]: layoutState.isSidebarOpened,
            })}
          >
            <div className={classes.fakeToolbar} />
            <Switch>
              <Route path="/app/requests" component={UserRequests} />
              <Route path="/app/reports" component={Reports} />
              <Route path="/app/manage" component={ManageUsers} />
              <Route
                exact
                path="/app/ui"
                render={() => <Redirect to="/app/ui/icons" />}
              />
              
            </Switch>
          </div>
        </>
    </div>
  );
}

export default withRouter(Layout);
